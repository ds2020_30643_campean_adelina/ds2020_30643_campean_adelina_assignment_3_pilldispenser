package rpc;

import controller.ClientController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

import java.text.ParseException;


@SpringBootApplication
public class ClientApp {


    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl("http://localhost:8080/sendinfo");
        invoker.setServiceInterface(PillDispenser.class);
        return invoker;
    }

    public static void crashString(String message, ClientController clientController) {

        try {
            clientController.getClientView().getData(message);

        } catch (ParseException p)
        {
            p.printStackTrace();
        }
    }

    public static String receivedMessage(String message) {
        return message;
    }

    public static void main(String[] args) {

        String mess = new String();

        ClientController clientController = new ClientController();

        ConfigurableApplicationContext context = SpringApplication.run(ClientApp.class, args);
        System.out.println("========Client Side===============");
        PillDispenser helloWorld = context.getBean(PillDispenser.class);

        String message = helloWorld.sendMessage("hei");

        crashString(message,clientController);

        clientController.createMessage(helloWorld);


        System.out.println(helloWorld.sendMessage("hei"));
    }
}