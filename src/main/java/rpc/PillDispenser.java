package rpc;

public interface PillDispenser {
    public String sendMessage(String msg);
    public void clientToServer(String message);
}
