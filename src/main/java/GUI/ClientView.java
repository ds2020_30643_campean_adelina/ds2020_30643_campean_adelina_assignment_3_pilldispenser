package GUI;


import rpc.PillDispenser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.GregorianCalendar;

public class ClientView extends Component {

    private JFrame frame = new JFrame();
    JPanel panel;

    private JLabel lblName = new JLabel("The medical plan contains the next medications!");
    private String[] medsInfo;
    JLabel lblClock = new JLabel();
    private SimpleDateFormat sdf;
    private Date dataC=new Date();
    private  Date current = new Date();

    PillDispenser pillDispenser;
    Date formatterStart;
    Date formatterEnd;

    public void setMessage(PillDispenser pillDispenser) {
        this.pillDispenser = pillDispenser;
    }

    public ClientView() {


        frame.setResizable(false);
        frame.setBounds(100, 100, 1200, 700);
        frame.setLocationRelativeTo(null);

        panel = new JPanel();
        panel.setLayout(null);

        panel.revalidate();

        frame.getContentPane().add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(false);
    }

    public void getData(String message) throws ParseException {

        panel.setLayout(null);
        System.out.println(message);
        String[] value2 = message.split(";");
        medsInfo = value2;

        System.out.println("size "+medsInfo.length);

        lblName.setForeground(new Color(0, 144, 255));
        lblName.setFont(new Font("Times New Roman", Font.BOLD, 40));
        lblName.setBounds(200, 10, 900, 50);
        panel.add(lblName);

        int k=70;
        for(int i=0; i<medsInfo.length; i++) {
            if (i > 0)
                k += 40;

            JTextArea textarea = new JTextArea();
            textarea.setBounds(70, 75 + k, 400, 30);

            textarea.setText(medsInfo[i]);
            textarea.setFont(new java.awt.Font("Dialog", 0, 20));

            textarea.setEditable(false);


            JButton button1 = new JButton("Medicine Taken");
            button1.setBounds(470, 75 + k, 200, 30);
            button1.setFont(new java.awt.Font("Dialog", 0, 20));
            button1.setBorderPainted(false);
            button1.setFocusable(false);
            button1.setForeground(new java.awt.Color(255, 255, 255));
            button1.setBackground(new java.awt.Color(0, 140, 255));
            button1.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseEntered(java.awt.event.MouseEvent evt) {
                    button1.setBackground(new java.awt.Color(6, 60, 104));
                }

                public void mouseExited(java.awt.event.MouseEvent evt) {
                    button1.setBackground(new java.awt.Color(0, 140, 255));
                }
            });
            panel.add(textarea);
            panel.add(button1);//, BorderLayout.PAGE_END);

            String[] inter = medsInfo[i].split("\\s+");

            button1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    //String[] interval = medsInfo[i].split("\\s+");
                    String[] hoursMed = inter[0].split("-");

                    Calendar calendarS = Calendar.getInstance();
                    try {
                        formatterStart = new SimpleDateFormat("HH:mm:ss").parse(hoursMed[0] + ":00");
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }

                    Calendar calendarE = Calendar.getInstance();
                    try {
                        formatterEnd = new SimpleDateFormat("HH:mm:ss").parse(hoursMed[1] + ":00");
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }

                    if (formatterStart.before(current) && formatterEnd.after(current)) {
                //se poate lua medicamentul

                        ImageIcon icon = new ImageIcon("src/images/meds.png");
                        JOptionPane.showMessageDialog(panel, "The medicine has been taken! " + "\n" + "Be safe!",
                                "Customized Dialog", JOptionPane.INFORMATION_MESSAGE, icon);
                        String msg = "The medication has been taken!";
                        pillDispenser.clientToServer(msg);
                        //button1.setBorderPainted( false );
                        //button1.setFocusPainted( false );
                        panel.remove(textarea);
                        panel.remove(button1);

            } else if (formatterStart.after(current)) {

                JOptionPane.showMessageDialog(panel, "It's not time yet, for this med!", "Error", JOptionPane.ERROR_MESSAGE);

                        String msg = "It's not time for this medication";
                        pillDispenser.clientToServer(msg);

            } else if (formatterEnd.before(current)) {

                System.out.println("end "+formatterEnd);

                JOptionPane.showMessageDialog(panel, "You forgot to take your meds!" + "\n" + "Your doctor has been notified!", "Error", JOptionPane.ERROR_MESSAGE, null);

                String msg = "The medication hasn't been taken!";
                pillDispenser.clientToServer(msg);
            }

                }
            });


            Timer timer = new Timer(1000, new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                     sdf = new SimpleDateFormat("HH:mm:ss");
                     dataC = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                    String hour = formatter.format(dataC);
                    try {
                        current = new SimpleDateFormat("HH:mm:ss").parse(hour);


                    }catch (ParseException p ){
                        p.printStackTrace();
                    }
                    lblClock.setText(sdf.format(dataC));

                }
            });

            timer.setInitialDelay(0);
            timer.start();

            lblClock.setFont(new Font("Times New Roman", Font.BOLD, 60));
            lblClock.setBounds(800, 250, 500, 100);
            panel.add(lblClock);

        }
        frame.setVisible(true);

    }

}
