package controller;

import GUI.ClientView;
import rpc.PillDispenser;

public class ClientController {

    ClientView clientView;
    PillDispenser pillDispenser;

    public ClientController() {
        this.clientView = new ClientView();
    }

    public void createMessage(PillDispenser pillDispenser){
        this.pillDispenser = pillDispenser;
        this.clientView.setMessage(pillDispenser);
    }

    public ClientView getClientView() {
        return clientView;
    }

    public void setClientView(ClientView clientView) {
        this.clientView = clientView;
    }
}
